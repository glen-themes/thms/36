$(document).ready(function(){
    // in customization page, show photosets even if they're stretched
    if($(".tumblr_preview_marker___").length){
        $(".photoset-grid div").each(function(){
            var hhheight = $(this).attr("data-height");
            $(this).height(hhheight)
        });
    }
    
    // remove tumblr preview marker by alyssa @thm97
    $(".tumblr_preview_marker___").remove();
    
    // once photosets are shown, remove the height
    if(!$(".tumblr_preview_marker___").length){
        $(".photoset-grid div").each(function(){
            $(this).css("height",null);
        });
    }
    
    // remove margins from empty <p>s
    $(".post p").each(function(){
        $(this).find("br:only-child").addClass("ok");
        
        $(this).contents().filter(function(){
          return this.nodeType === 3;
        }).wrap( "<span>" );
        
        if($(this).is(":empty")){
            $(this).remove()
        }
    });
        
    $(".ok").each(function(){
        if(!$(this).siblings().length){
            $(this).parent("p").remove();
        }
    });
    
    // <figure> image in reblogs
    // if the last element is an image, double the padding
    // so that the border-bottom doesn't look awkward
    var captions_gap = parseInt(getComputedStyle(document.documentElement)
       .getPropertyValue("--Captions-Gap"));
    
    // if commenter is first-child, remove top whitespace
    $(".holyshit .commenter, .asker_").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top",0)
        }
    });
    
    // remove headings whitespace
    $("h1, h2, h3, h4, h5, h6").each(function(){
        if(!$(this).parents(".comment_container").next().length){
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        }
    });
    
    $("h1.title, .post h2").each(function(){
        if(!$(this).prev().length){
            $(this).css("margin-top","0")
        }
    });
    
    // responsive videos & iframes
    flexibleFrames($(".video"));
    
    // turn npf_chat into a chat post
    $(".npf_chat").addClass("chatholder body").removeClass("npf_chat");
    
    $(".chatholder").each(function(){
        if($(this).attr("data-npf")){
            $(this).find("b").addClass("chat_label");
            $(this).contents().last().wrap("<span class='chat_content'>")
        }
    });
    
    // add colon at the end of each chat_label
    $(".chat_label").each(function(){
        if(!$(this).text().endsWith(":")){
            $(this).append(":")
        }
    });
    
    // if:reblog dividers, increase space between 1st commenter & the next
    var p_gap = getComputedStyle(document.documentElement)
               .getPropertyValue("--Paragraph-Margins"),
        numonly = parseFloat(p_gap),
        suffix = p_gap.split(/\d/).pop();
    
    $(".oui p").each(function(){
        if($(this).parents(".comment_container").next().length){
            $(this).css("margin-bottom",numonly / 2 + suffix)
        }
    });


    fetch("//glen-themes.gitlab.io/thms/36/freepik_repeat.html")
    .then(rt_svg => {
      return rt_svg.text()
    })
    .then(rt_svg => {
      $(".retweet").html(rt_svg)
    });
    
    
    fetch("//glen-themes.gitlab.io/thms/36/freepik_like.html")
    .then(like_svg => {
      return like_svg.text()
    })
    .then(like_svg => {
      $(".heart").html(like_svg)
    });
    
    
    fetch("//glen-themes.gitlab.io/thms/36/like_solid.html")
    .then(like_svg => {
      return like_svg.text()
    })
    .then(like_svg => {
      $(".heart_sld").html(like_svg)
    });
    
    // show OP source if post does not have a caption
    $(".src").each(function(){
        $(this).parents(".post").prepend($(this));
        $(this).css("margin-top",0);
        $(this).css("margin-bottom",captions_gap);
        $(this).show();
    });
    
    // hide duplicate OP source if caption(s) exist
    $(".post").each(function(){
        if($(this).find(".comment_container").length || $(this).find("body").length || $(this).find(".question_container").length){
            $(this).find(".src").hide();
        }
    });
    
    // hide 'via' on quote posts
    $(".quote-source").each(function(){
        var qs = $(this).html();
        var crop = qs.split('(via')[0];
        var again = crop.split('<br>')[0];
        $(this).html(again)
    });
    
    // hide commas in 'k' note count
    $(".notecount a, .noteperma").each(function(){
        var con = $(this).text().replace(/[,]/g,'');
        $(this).text(con);
        
        // wrap numbers script
        // https://stackoverflow.com/a/46980974/8144506
        var $num = $(this);
        var modifiedText = $num.text().replace(/\d+/g, '<span class="num">$&</span>');
        $num.html(modifiedText);
    });
    
    // randomize PostID number
    // https://stackoverflow.com/a/13365977/8144506
    $(".barcode span").each(function(){
        var bc = $(this).text().split('')
                .sort(function(){
                    return 0.5-Math.random();
                }).join('');
        $(this).text(bc)
    });
    
    // if user has default t*mblr icon
    $(".commenter img").each(function(){
        var df = $(this).attr("src");
        if (df.indexOf('default_avatar') > -1) {
            $(this).wrap("<div class='avr'></div>");
            $(this).remove();
        }
    });
    
    // if user has deactivated
    $(".commenter span").each(function(){
        if(!$(this).children("a").length){
            $(this).siblings("img").wrap("<div class='avr'></div>");
        };
    });
    
    $(".avr").each(function(){
        $(this).children("img").remove();
        $("<div class='avc'></div>").appendTo($(this));
    });
    
    $(".avc").each(function(){
        $("<div class='avez'></div>").appendTo($(this));
    });
    
    // make space for the scrollbar
    var scrb = parseInt(getComputedStyle(document.documentElement)
               .getPropertyValue("--Scrollbar-Padding"));
    
    if($("#horizontal-c").height() > $("#horizontal-b").height()){
        $(".main").css("margin-right",scrb/-2);
        $(".shrimp.right").css("padding-right",scrb/2);
    }
    
    // prevent barcode from intersecting with permalink text
    var peew = $(".permawrap").eq(0).width();
    
    var peepee = parseInt(getComputedStyle(document.documentElement)
                .getPropertyValue("--Post-Buttons-Size"));
    
    $(".notecount").each(function(){
        
        var nc = $(this).width();
        var ch = ((peew - (peepee * 2) - nc) * 0.69);
        
        $(this).siblings(".les-controls").find(".barcode span")
        .css("width",ch);
    });
    
    // get custom links line width
    // apply slowed transition to longer lines
    $(".bloop").css("flex","1");
    
    $(window).load(function(){
        $(".bloop").each(function(){
            var adult = $(".linques").width();
            var lemgth = $(this).width();
            var hondred = 1 - ((100 - lemgth) * 0.01);
            var toddhoward = (hondred * 0.69).toFixed(1);
            
            var h4lf = adult * 0.4;
            if(lemgth > h4lf){
                 $(this).css("transition-duration",toddhoward + "s");
            }
            
            $(this).css("flex","");
        });
    });
    
    // set pagination location
    var pagiwidth = $(".pagi").width();
    var pmarge = parseInt(getComputedStyle(document.documentElement)
                .getPropertyValue("--NextPage-Button-Margin"));
    $(".pagi").css("margin-right",-pagiwidth - pmarge);
    
    
    $('.mask img, .trans img').each(function(){
         var ghostsrc = $(this).attr('src');
         if(ghostsrc.indexOf("static.tumblr.com/2pnwama/J3Cs1xaj0/jack_render.png") > -1){
             $(this).css("padding-left","0")
         }
    });
    
});//end ready
