// jack from ghostrunner is trans

$(document).ready(function(){
    $(".tumblr_preview_marker___").remove();
    
    $(window).load(function(){
        $(".haachoo._TOP").each(function(){
            var xo = parseInt($(this).css("min-width")) * 1.169;
            $(this).css("max-width",xo)
        });
    });
    
    //**** LOADING SCREEN STUFF
    fetch('https://akveo.github.io/eva-icons/outline/svg/loader-outline.svg')
    .then(data => {
      return data.text()
    })
    .then(data => {
      var divs = document.getElementsByClassName("beload");
        for (var i = 0; i < divs.length; i++){
            divs[i].innerHTML = data;
        }
    });//end fetch
    
    var ele = document.getElementById('yokotaro');
    
    if(window.location.href.indexOf("/customize") > -1){
        setTimeout(function(){
            ele.style.transition = '369ms';
            ele.style.opacity = 0;
            
            setTimeout(function(){
                ele.style.display = "none";
            },369);
        },699);
    } else {
        window.onload = function(){
            $("#yokotaro").fadeOut(260);
        };//end winload
    };
    
    //**** SET EVEN HEIGHTS FOR MINI SQUARES IN CORNER
    
    var eggy = $(".oeuf").height();
    var spac = parseInt(getComputedStyle(document.documentElement)
               .getPropertyValue("--Corner-Boxes-Dots-Spacing"));
    var indiv = ((eggy - (spac * 2)) / 3);
    
    
    
    $(".bit").height(indiv).width(indiv);
    
    // load squares
    $(".shrimp.left .squares").html("aac++<br>acc++<br>cac++<br>aacc+<br>aacac<br>+acca");
    $(".shrimp.right .squares").html("++caa<br>++cca<br>++cac<br>+ccaa<br>cacaa<br>acca+");
    
    //**** LOADING SCREEN STUFF
    $(".squares").each(function(){
        var getsquares = $(this).html();
        var sqrpc = getsquares.replace(/[ ]/g,'')
                              .replace(/[c]/g,'<span class="fade">a</span>')
                              .replace(/[+]/g,'<span class="null">a</span>');
        $(this).html(sqrpc);
    });
    
    
    //**** MAKE FRAME SOLID IF GRADIENT IS 0
    var grad = parseInt(getComputedStyle(document.documentElement)
               .getPropertyValue("--Inner-Frame-Bottom-Fade-Amount"));
               
    if(grad == 0) {
        $("[class^='shortboi'], [class^='botsides']").addClass("solid");
    }
    
    //**** RELEASE THE SVGS (did I really just say that?)
    $(".one-link").each(function(){
        $(this).prepend('<span class="wood"></span>');
    });
    
    var svg_divs = '\
        <span class="bg fr"></span>\
        <span class="bg bk"></span>\
        <span class="bg tt"></span>\
        <span class="bg rr"></span>\
        <span class="bg bm"></span>\
        <span class="bg vz"></span>\
    ';
    
    $(".wood").prepend(svg_divs);
    
    
    fetch('//glen-themes.gitlab.io/thms/36/bg-mask-svg.html')
    .then(data => {
      return data.text()
    })
    .then(data => {
      var divs = document.getElementsByClassName("bg");
        for (var i = 0; i < divs.length; i++){
            divs[i].innerHTML = data;
            
            $(".bg").each(function(){
                var getname = $(this).attr("class").split(" ").pop();
                $(this).find("path").attr("id",getname);
                $(this).find("use").attr("xlink:href","#" + getname);
            });
            
            var x = $(".strawberry").width();
            $(".haachoo._TOP").css("min-width",x)
        }
        
    });//end fetch
    
    /*---------------- MUSIC PLAYER ----------------*/
    $(".song:first").addClass("current");
    
    $(".prev").click(function(){
        $(".song").filter(".current").hide().removeClass("current").prev(".song").show().addClass("current");
        if(!$(".song").hasClass("current")){
            $(".song:last").addClass("current").show();
        }
    });//end prev
    
    $(".next").click(function(){
        $(".song").filter(".current").hide().removeClass("current").next(".song").show().addClass("current");
        if(!$(".song").hasClass("current")){
            $(".song:first").addClass("current").show();
        }
    });//end next
    
    $(".prev, .next, .playbutt").click(function(){
        var uhhh = $(".current").find("audio")[0];
        if (uhhh.paused) {
            uhhh.play();
            $(".pausee").show();
            $(".playy").hide();
        } else {
            uhhh.pause();
            $(".playy").show();
            $(".pausee").hide();
        }
    
        document.addEventListener("play", function(e){
        var audios = document.getElementsByTagName("audio");
            for(var i = 0, len = audios.length; i < len;i++){
                if(audios[i] != e.target){
                    audios[i].pause();
                    audios[i].currentTime = 0;
                }
            }
        }, true);
    });
    
    // when each song ends, jumps to the next one 
    $(".song").find("audio").each(function(){
        $(this).bind("ended", function(){
            $(".song").filter(".current").hide().removeClass("current").next(".song").show().addClass("current");
            var uhhh = $(".current").find("audio")[0];
            if (uhhh.paused) {
                uhhh.play();
                $(".pausee").show();
                $(".playy").hide();
            } else {
                uhhh.pause();
                $(".playy").show();
                $(".pausee").hide();
            }
        });
    });
    
    // when playlist has finished, stop all music and show first song
     $(".song:last").find("audio")[0].onended = function(){
        $(".playy").show();
        $(".pausee").hide();
            
        $(".song").filter(".current").hide().removeClass("current").next(".song").show().addClass("current");
        if(!$(".song").hasClass("current")){
            $(".song:first").addClass("current").show();
        }
    }
    
    // if none of the songs have names, remove height
    $(".song").addClass("hasname");
    
    $(".song").each(function(){
        if(!$(this).find("span").length){
            $(this).removeClass("hasname");
        }
    });
    
    $(".song span").each(function(){
        
        if($(this).is(":empty")){
            $(this).parents(".song").removeClass("hasname");
        }
    });
    
    if(!$(".song").hasClass("hasname")){
        $(".tracklist").addClass("nil");
    }
    
    
    let root = document.documentElement;
    var berry = $(".strawberry").width();
    var bspc = parseInt(getComputedStyle(document.documentElement)
               .getPropertyValue("--TopLinks-Spacing"));
    
    root.style.setProperty('--TopLinks-Bump-Length', (berry * 1.069) + (bspc*4) + "px");
    
    
});//end ready
