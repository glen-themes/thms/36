![Screenshot preview of the theme "Glitch" by glenthemes](https://64.media.tumblr.com/a46b863cf18f63ea941419c8073737e6/a80749964cfee4fc-d6/s1280x1920/39d5fca90ee0c1c4549737391c6181f8c33d0750.png)

**Theme no.:** 36  
**Theme name:** Glitch  
**Theme type:** Free / Tumblr use  
**Description:** A cyberpunk theme inspired by [Ghostrunner](https://ghostrunnergame.com/), a fast-paced first-person slasher where the protagonist tears his way to unveil the truth about the post-apocalyptic world — and himself. All shapes crafted from CSS ⊰ (〃´∀｀〃).  
**Author:** @&hairsp;glenthemes  

**Release date:** 2020-12-25

**Post:** [glenthemes.tumblr.com/post/638498668143656960](https://glenthemes.tumblr.com/post/638498668143656960)  
**Preview:** [glenthpvs.tumblr.com/glitch](https://glenthpvs.tumblr.com/glitch)  
**Download:** [pastebin.com/P9thrSji](https://pastebin.com/P9thrSji)  
**Credits:** [glenthemes.tumblr.com/glitch-crd](https://glenthemes.tumblr.com/glitch-crd)
